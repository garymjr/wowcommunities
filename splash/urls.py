from django.urls import path

from splash import views

app_name = "splash"
urlpatterns = [path("", views.index, name="index")]
